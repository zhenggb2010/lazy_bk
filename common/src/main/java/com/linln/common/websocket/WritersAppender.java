package com.linln.common.websocket;


import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;

import javax.websocket.Session;
import java.io.Serializable;


@Plugin(name = "WritersAppender", category = "Core", elementType = "appender", printObject = true)
public class WritersAppender extends AbstractAppender {



    public Session session;
    /* 构造函数 */
    public WritersAppender(Session session,String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
        this.session =session;
    }

    @Override
    public void append(LogEvent event) {
        try {
              session.getBasicRemote().sendText(new String(getLayout().toByteArray(event)));
        } catch (Exception e) {
            LOGGER.error("write file exception", e);
        }
    }



//    @PluginFactory
//    public static WritersAppender createAppender(Writer writer,@PluginAttribute("name") String name,
//                                                     @PluginElement("Layout") Layout<? extends Serializable> layout,
//                                                     @PluginElement("Filter") final Filter filter, @PluginAttribute("sysCode") String sysCode,
//                                                     @PluginAttribute("period") String period) {
//        if (name == null) {
//            LOGGER.error("No name provided for MyCustomAppenderImpl");
//            return null;
//        }
//        if (layout == null) {
//            layout = PatternLayout.createDefaultLayout();
//        }
//        return new WritersAppender(writer,name, filter, layout, false);
//    }

}