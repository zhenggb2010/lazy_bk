package com.linln.modules.system.repository;

import com.linln.modules.system.domain.About;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/02/13
 */
public interface AboutRepository extends BaseRepository<About, Long> {
}