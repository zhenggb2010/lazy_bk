package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Cate;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
public interface CateRepository extends BaseRepository<Cate, Long> {
}