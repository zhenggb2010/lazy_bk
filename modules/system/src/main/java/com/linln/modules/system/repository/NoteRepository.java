package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Note;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
public interface NoteRepository extends BaseRepository<Note, Long> {
}